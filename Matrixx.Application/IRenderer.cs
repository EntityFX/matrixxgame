﻿using System.Drawing;

namespace Matrixx.Application
{
    public interface IRenderer
    {
        void Render(IMatrixx game);

        Bitmap Bitmap { get; }
    }
}