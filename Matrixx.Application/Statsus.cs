﻿namespace Matrixx.Application
{
    public enum Statsus
    {
        Added,
        CannotAdd,
        GameEnded
    }
}