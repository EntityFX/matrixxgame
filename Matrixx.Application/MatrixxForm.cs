﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Matrixx.Application
{
    public partial class MatrixxForm : Form, INotifyPropertyChanged
    {
        private readonly IMatrixx _game;
        private MatrixxController _gameController;

        public MatrixxForm()
        {
            InitializeComponent();
            var renderer = new MatrixxRenderer();
            pictureBox1.Image = renderer.Bitmap;
            _game = new MatrixxCore(renderer);
            _game.AddFigure(_game.FigureSlots[1], new Point(1, 0));
            _gameController = new MatrixxController(_game);
            scoreValueLabel.DataBindings.Add("Text", _game, "Score", false, DataSourceUpdateMode.OnPropertyChanged);
            highScoreLabelValue.DataBindings.Add("Text", _game, "HighScore", false, DataSourceUpdateMode.OnPropertyChanged);
            StatusLabel.DataBindings.Add("Text", this, "Status", false, DataSourceUpdateMode.OnPropertyChanged);
        }

        private Statsus _status;
        public Statsus Status
        {
            get { return _status; }
            set
            {
                _status = value;
                OnPropertyChanged();
            }
        }

        private void MatrixxForm_KeyUp(object sender, KeyEventArgs e)
        {

            switch (e.KeyCode)
            {
                case Keys.Up:
                    _gameController.SelectCellUp();
                    break;
                case Keys.Down:
                    _gameController.SelectCellDown();
                    break;
                case Keys.Left:
                    _gameController.SelectCellLeft();
                    break;
                case Keys.Right:
                    _gameController.SelectCellRight();
                    break;
                case Keys.D1:
                case Keys.D2:
                case Keys.D3:
                    Status = _gameController.AddFigureFromSlot(e.KeyValue - 49);
                    break;
            }
            Refresh();
        }

        private void MatrixxForm_Paint(object sender, PaintEventArgs e)
        {
            _game.Render();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
