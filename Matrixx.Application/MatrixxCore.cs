﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Matrixx.Application
{
    public class MatrixxCore : IMatrixx, INotifyPropertyChanged
    {
        private const int RowsCount = 10;
        private const int ColsCount = 10;

        private byte[,] _field = new byte[RowsCount, ColsCount];
        private readonly Figure[] _figureSlots = new Figure[3];
        private Random _generator = new Random((int)DateTime.Now.Ticks);
        private readonly IRenderer _renderer;

        private readonly IReadOnlyDictionary<int, Figure> _figures = new Dictionary<int, Figure>
        {
            {
                1, new Figure(new byte[,]{{1}}) { ColorIndex = 1}
            },
            {
                2, new Figure(new byte[,]{{1, 1, 1, 1}}) { ColorIndex = 2}
            },
            {
                3, new Figure(new byte[,]{
                    {1, 1},
                    {1, 1}
                }) { ColorIndex = 3}
            },
            {
                4, new Figure(new byte[,]{
                    { 1 },
                    { 1 },
                    { 1 },
                    {1}
                }) { ColorIndex = 4}
            },
            {
                5, new Figure(new byte[,]{
                    { 1, 0, 0 },
                    { 1 ,  1 , 1}
                }) { ColorIndex = 5}
            },
            {
                6, new Figure(new byte[,]{
                    { 0, 0, 1 },
                    { 1 ,  1 , 1}
                }) { ColorIndex = 6}
            },
            {
                7, new Figure(new byte[,]{
                    { 1, 1, 1 },
                    { 1 ,  0 , 0}
                }) { ColorIndex = 7}
            },
            {
                8, new Figure(new byte[,]{
                    { 1, 1, 1 },
                    { 0 ,  0 , 1}
                }) { ColorIndex = 8}
            },
        };

        private uint _score;

        public uint Score
        {
            get
            {
                return _score;
            }
            private set
            {
                _score = value;
                if (_score > _highScore)
                {
                    _highScore = _score;
                }
                OnPropertyChanged();
            }
        }

        private uint _highScore;
        public uint HighScore
        {
            get { return _highScore; }
            private set
            {
                _highScore = value;
                OnPropertyChanged();
            }
        }

        public Point SelectedCell { get; set; }

        public byte[,] Field
        {
            get
            {
                return _field;
            }
        }

        public Figure[] FigureSlots
        {
            get { return _figureSlots; }
        }

        public MatrixxCore(IRenderer renderer)
        {
            _renderer = renderer;
            GenerateFigureSlots();
        }

        public void Render()
        {
            _renderer.Render(this);
        }

        public void Reset()
        {
            GenerateFigureSlots();
            Score = 0;
            _field = new byte[RowsCount, ColsCount];
        }

        public Statsus AddFigure(Figure figure, Point coords)
        {
            int dx = coords.X, dy = coords.Y;
            if (!CanAdd(figure, coords))
            {
                return Statsus.CannotAdd;
            }
            for (int fy = 0; fy < figure.Pattern.GetLength(1); fy++)
            {
                for (int fx = 0; fx < figure.Pattern.GetLength(0); fx++)
                {
                    if (figure.Pattern[fx, fy] == 1)
                    {
                        _field[dx, dy] = figure.ColorIndex;
                    }
                    dx++;
                }
                dx = coords.X;
                dy++;
            }
            ValidateFigureSlots(figure);
            Score += GetPointsByFigure(figure);
            Score += ValidateRowsOrColsClosed();
            return CanPlay() ? Statsus.Added : Statsus.GameEnded;
        }

        private void ValidateFigureSlots(Figure figure)
        {
            var index = Array.FindIndex(FigureSlots, f => f == figure);
            FigureSlots[index] = null;
            if (FigureSlots.All(f => f == null))
            {
                GenerateFigureSlots();
            }
        }

        private uint ValidateRowsOrColsClosed()
        {
            var counter = 0;
            uint points = 0;
            for (int fx = SelectedCell.X; fx < _field.GetLength(0); fx++)
            {
                for (int iy = 0; iy < _field.GetLength(1); iy++)
                {
                    if (_field[fx, iy] > 0)
                    {
                        counter++;
                    }
                }
                if (counter == _field.GetLength(1))
                {
                    for (int iy = 0; iy < _field.GetLength(1); iy++)
                    {
                        _field[fx, iy] = 0;
                    }
                    points += (uint)_field.GetLength(1);
                }
                counter = 0;
            }
            for (int fy = 0; fy < _field.GetLength(1); fy++)
            {
                for (int ix = 0; ix < _field.GetLength(0); ix++)
                {
                    if (_field[ix, fy] > 0)
                    {
                        counter++;
                    }
                }
                if (counter == _field.GetLength(1))
                {
                    for (int ix = 0; ix < _field.GetLength(0); ix++)
                    {
                        _field[ix, fy] = 0;
                    }
                    points += (uint)_field.GetLength(0);
                }
                counter = 0;
            }
            return points;
        }

        private bool CanAdd(Figure figure, Point coords)
        {
            int dx = coords.X, dy = coords.Y;
            for (int fy = 0; fy < figure.Pattern.GetLength(1); fy++)
            {
                for (int fx = 0; fx < figure.Pattern.GetLength(0); fx++)
                {
                    if (dx >= _field.GetLength(0) || dy >= _field.GetLength(1))
                    {
                        return false;
                    }
                    if (figure.Pattern[fx, fy] == 1 && _field[dx, dy] > 0)
                    {
                        return false;
                    }
                    dx++;
                }
                dx = coords.X;
                dy++;
            }
            return true;
        }

        private bool CanPlay()
        {
            for (int iy = 0; iy < _field.GetLength(1); iy++)
            {
                for (int ix = 0; ix < _field.GetLength(0); ix++)
                {
                    foreach (var figure in FigureSlots)
                    {
                        if (figure != null)
                        {
                            if (CanAdd(figure, new Point(ix, iy)))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        private uint GetPointsByFigure(Figure figure)
        {
            uint points = 0;
            for (int fy = 0; fy < figure.Pattern.GetLength(1); fy++)
            {
                for (int fx = 0; fx < figure.Pattern.GetLength(0); fx++)
                {
                    if (figure.Pattern[fx, fy] == 1)
                    {
                        points++;
                    }
                }
            }
            return points;
        }

        private void GenerateFigureSlots()
        {
            var keys = _figures.Keys.ToArray();
            for (int t = 0; t < _figureSlots.Length; t++)
            {
                var tmp = keys[t];
                int r = _generator.Next(t, keys.Length);
                keys[t] = keys[r];
                keys[r] = tmp;
            }

            for (int i = 0; i < _figureSlots.Length; i++)
            {
                _figureSlots[i] = _figures[keys[i]];
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
