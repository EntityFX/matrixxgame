﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Matrixx.Application
{
    public partial class MatrixxForm
    {
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ScoreLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.scoreValueLabel = new System.Windows.Forms.Label();
            this.highScoreLabelValue = new System.Windows.Forms.Label();
            this.highScoreLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ScoreLabel
            // 
            this.ScoreLabel.AutoSize = true;
            this.ScoreLabel.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ScoreLabel.Location = new System.Drawing.Point(12, 9);
            this.ScoreLabel.Name = "ScoreLabel";
            this.ScoreLabel.Size = new System.Drawing.Size(64, 23);
            this.ScoreLabel.TabIndex = 0;
            this.ScoreLabel.Text = "Score";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(0, 44);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Padding = new System.Windows.Forms.Padding(20);
            this.pictureBox1.Size = new System.Drawing.Size(437, 329);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StatusLabel.Location = new System.Drawing.Point(305, 9);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(68, 23);
            this.StatusLabel.TabIndex = 3;
            this.StatusLabel.Text = "Ready";
            // 
            // scoreValueLabel
            // 
            this.scoreValueLabel.AutoSize = true;
            this.scoreValueLabel.Font = new System.Drawing.Font("Verdana", 14.25F);
            this.scoreValueLabel.Location = new System.Drawing.Point(82, 9);
            this.scoreValueLabel.Name = "scoreValueLabel";
            this.scoreValueLabel.Size = new System.Drawing.Size(22, 23);
            this.scoreValueLabel.TabIndex = 1;
            this.scoreValueLabel.Text = "0";
            // 
            // highScoreLabelValue
            // 
            this.highScoreLabelValue.AutoSize = true;
            this.highScoreLabelValue.Font = new System.Drawing.Font("Verdana", 14.25F);
            this.highScoreLabelValue.Location = new System.Drawing.Point(244, 9);
            this.highScoreLabelValue.Name = "highScoreLabelValue";
            this.highScoreLabelValue.Size = new System.Drawing.Size(22, 23);
            this.highScoreLabelValue.TabIndex = 5;
            this.highScoreLabelValue.Text = "0";
            // 
            // highScoreLabel
            // 
            this.highScoreLabel.AutoSize = true;
            this.highScoreLabel.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.highScoreLabel.Location = new System.Drawing.Point(130, 9);
            this.highScoreLabel.Name = "highScoreLabel";
            this.highScoreLabel.Size = new System.Drawing.Size(108, 23);
            this.highScoreLabel.TabIndex = 4;
            this.highScoreLabel.Text = "HighScore";
            // 
            // MatrixxForm
            // 
            this.ClientSize = new System.Drawing.Size(436, 382);
            this.Controls.Add(this.highScoreLabelValue);
            this.Controls.Add(this.highScoreLabel);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.scoreValueLabel);
            this.Controls.Add(this.ScoreLabel);
            this.Name = "MatrixxForm";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MatrixxForm_Paint);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MatrixxForm_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private PictureBox pictureBox1;
        private Label ScoreLabel;
        private Label StatusLabel;
        private Label scoreValueLabel;
        private Label highScoreLabelValue;
        private Label highScoreLabel;
    }
}
