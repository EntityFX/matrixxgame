﻿using System.Collections.Generic;
using System.Drawing;

namespace Matrixx.Application
{
    public class MatrixxRenderer : IRenderer
    {
        private readonly Color _emptyColor;

        private const int CellWidth = 30;
        private const int CellHeight = 30;
        private const int MariginX = 2;
        private const int MariginY = 2;

        private Color[] Colors { get; } = {
            Color.FromArgb(26, 188, 156),
            Color.FromArgb(46, 204, 113),
            Color.FromArgb(52, 152, 219),
            Color.FromArgb(155, 89, 182),
            Color.FromArgb(52, 73, 94),
            Color.FromArgb(241, 196, 15),
            Color.FromArgb(230, 126, 34),
            Color.FromArgb(231, 76, 60),
            Color.FromArgb(189, 195, 199),
            Color.FromArgb(76, 76, 76)
        };

        private readonly IReadOnlyDictionary<int, int> _colorIndexes = new Dictionary<int, int>
        {
            {1, 0},{2, 2},{3, 1},{4, 4},{5, 3},{6, 5},{7, 6},{8, 7}
        };

        public MatrixxRenderer()
        {
            Bitmap = new Bitmap(320, 450);
            _emptyColor = Colors[8];
        }

        public Bitmap Bitmap
        {
            get;
        }

        public void Render(IMatrixx game)
        {
            RenderField(game);
            RenderFiguresSlot(game);
        }

        private void RenderField(IMatrixx game)
        {
            int dx = 0, dy = 0;
            using (var graphics = Graphics.FromImage(Bitmap))
            {
                graphics.Clear(Color.AliceBlue);
                using (var brush = new SolidBrush(Colors[0]))
                using (var emptyCellBrush = new SolidBrush(_emptyColor))
                {
                    for (int cy = 0; cy < game.Field.GetLength(1); cy++)
                    {
                        for (int cx = 0; cx < game.Field.GetLength(0); cx++)
                        {
                            var cell = game.Field[cx, cy];
                            Brush currentBrush;
                            if (cell > 0)
                            {
                                brush.Color = Colors[_colorIndexes[cell]];
                                currentBrush = brush;
                            }
                            else
                            {
                                currentBrush = emptyCellBrush;
                            }
                            graphics.FillRectangle(currentBrush, dx, dy, CellWidth, CellHeight);
                            dx = dx + CellWidth + MariginX;
                        }
                        dx = 0;
                        dy = dy + CellHeight + MariginY;
                    }
                }

                using (var selectedPen = new Pen(Colors[9], 2))
                {
                    graphics.DrawRectangle(selectedPen,
                        game.SelectedCell.X * CellWidth + game.SelectedCell.X * MariginX + 1,
                        game.SelectedCell.Y * CellHeight + game.SelectedCell.Y * MariginY + 1,
                        CellWidth - 2, CellHeight - 2);
                }
            }
        }

        private void RenderFiguresSlot(IMatrixx game)
        {

            int sx = 0, sy = game.Field.GetLength(1) * CellHeight + game.Field.GetLength(1) * MariginY + CellHeight;
            int sdy = sy, sdx = sx;
            int slotCellWidth = 20, slotCellHeight = 20;
            int slotMarginx = 2, slotMarginy = 2;
            using (var graphics = Graphics.FromImage(Bitmap))
            using (var brush = new SolidBrush(Colors[0]))
            {
                foreach (var gameFigureSlot in game.FigureSlots)
                {
                    if (gameFigureSlot != null)
                    {
                        Color figureColor = Colors[_colorIndexes[gameFigureSlot.ColorIndex]];
                        for (int fy = 0; fy < gameFigureSlot.Pattern.GetLength(1); fy++)
                        {
                            for (int fx = 0; fx < gameFigureSlot.Pattern.GetLength(0); fx++)
                            {
                                var figureCell = gameFigureSlot.Pattern[fx, fy];
                                if (figureCell > 0)
                                {
                                    brush.Color = figureColor;
                                    graphics.FillRectangle(brush, sdx, sdy, slotCellWidth, slotCellHeight);
                                }
                                sdx = sdx + slotCellWidth + slotMarginx;
                            }
                            sdx = sx;
                            sdy = sdy + slotCellHeight + slotMarginy;
                        }
                    }

                    sx = sx + Bitmap.Width / game.FigureSlots.Length;
                    sdx = sx;
                    sdy = sy;
                }

            }
        }
    }
}
