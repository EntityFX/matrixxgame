﻿namespace Matrixx.Application
{
    public class Figure
    {
        public byte[,] Pattern { get; private set; }

        public byte ColorIndex { get; set; }

        public Figure(byte[,] pattern)
        {
            Pattern = pattern;
        }
    }
}
