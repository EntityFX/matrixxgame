﻿namespace Matrixx.Application
{
    public class MatrixxController
    {
        private readonly IMatrixx _game;

        public MatrixxController(IMatrixx game)
        {
            _game = game;
        }

        public void SelectCellUp()
        {
            if (_game.SelectedCell.Y > 0)
            {
                var newCoords = _game.SelectedCell;
                newCoords.Offset(0, -1);
                _game.SelectedCell = newCoords;
            }
        }

        public void SelectCellDown()
        {
            if (_game.SelectedCell.Y < _game.Field.GetLength(1) - 1)
            {
                var newCoords = _game.SelectedCell;
                newCoords.Offset(0, 1);
                _game.SelectedCell = newCoords;
            }
        }

        public void SelectCellLeft()
        {
            if (_game.SelectedCell.X > 0)
            {
                var newCoords = _game.SelectedCell;
                newCoords.Offset(-1, 0);
                _game.SelectedCell = newCoords;
            }
        }

        public void SelectCellRight()
        {
            if (_game.SelectedCell.X < _game.Field.GetLength(0) - 1)
            {
                var newCoords = _game.SelectedCell;
                newCoords.Offset(1, 0);
                _game.SelectedCell = newCoords;
            }
        }

        public Statsus AddFigureFromSlot(int slotNumber)
        {
            var figure = _game.FigureSlots[slotNumber];
            if (figure == null)
            {
                return Statsus.CannotAdd;
            }
            var status = _game.AddFigure(figure, _game.SelectedCell);
            if (status == Statsus.GameEnded)
            {
                _game.Reset();
            }
            return status;
        }
    }
}