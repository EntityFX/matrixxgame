﻿using System.Drawing;

namespace Matrixx.Application
{
    public interface IMatrixx
    {
        uint Score { get; }

        uint HighScore { get; }

        Point SelectedCell { get; set; }

        byte[,] Field{ get;}

        Figure[] FigureSlots { get; }

        Statsus AddFigure(Figure figure, Point coords);

        void Render();

        void Reset();
    }
}